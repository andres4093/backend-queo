<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('postId')->references('id')->on('posts');
            $table->foreign('userId')->references('id')->on('users');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('tagId')->references('id')->on('tags');
            $table->foreign('userId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
          $table->dropForeign('posts_tagid_foreign');
          $table->dropForeign('posts_userid_foreign');
        });

        Schema::table('comments', function (Blueprint $table) {
          $table->dropForeign('comments_postid_foreign');
          $table->dropForeign('comments_userid_foreign');
        });
    }
}
