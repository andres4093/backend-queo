<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Requests\User as UserRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(UserRequest $request)
    {
        /* Se extrae la data del registro y se asigna a una nueva variable */
        $input    = $request->all();
        /* Se guarda el comentario ingresado */
        $input['password']  = bcrypt($input['password']);
        $user               = User::create($input);
        $success['token']   = $user->createToken('MyApp')->accessToken;
        $success['name']    = $user->name;
        /* Retorno del registro junto con el la estructura del sendResponse */
        return $this->sendResponse($success, 'Usuario registrado.');
    }
}
