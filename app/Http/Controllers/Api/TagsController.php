<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Requests\Tags as TagsRequest;
use Illuminate\Http\Request;
use App\Models\Tags;

class TagsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /* Se traen todas las etiquetas */
      $tags = Tags::paginate(10);
      /* Retorna las etiquetas almacenadas junto con el la estructura del sendResponse */
      return $this->sendResponse($tags->toArray(), 'Etiquetas.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagsRequest $request)
    {
      /* Se extrae la data del registro y se asigna a una nueva variable */
      $input    = $request->all();
      /* Se guarda la etiqueta ingresada */
      $tag      = Tags::create($input);
      /* Retorno de la etiqueta junto con el la estructura del sendResponse */
      return $this->sendResponse($tag->toArray(), 'Etiqueta creada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* Busqueda de la etiqueta especifica */
        $tag = Tags::find($id);
        /* Validacion si no encuentra la etiqueta */
        if (is_null($tag)) {
            return $this->sendError('No existe la etiqueta.');
        }
        /* Retorno de la etiqueta especifica junto con el la estructura del sendResponse */
        return $this->sendResponse($tag->toArray(), 'Etiqueta retornada.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagsRequest $request, $id)
    {
        /* Busqueda de la etiqueta especifica */
        $tag = Tags::find($id);
        /* Se extrae la data de la etiqueta y se asigna a una nueva variable */
        $tag->update($request->all());
        /* Retorno de la etiqueta especifica junto con el la estructura del sendResponse */
        return $this->sendResponse($tag->toArray(), 'Etiqueta Actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /* Busqueda de la etiqueta especifica */
        $tag = Tags::find($request->id);
        /* Metodo para eliminar la etiqueta */
        $tag->delete();
        /* Retorno de la etiqueta eliminada junto con el la estructura del sendResponse */
        return $this->sendResponse($tag->toArray(), 'Etiqueta Eliminada.');
    }
}
