<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    /**
     * Metodo para respuesta del las peticiones exitosas.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
  	    $response = [
          'success' => true,      // verdadero cuando es exitoso
          'data'    => $result,   // informacion de respuesta de los metodos
          'message' => $message,  // mensaje que retorna la peticion
        ];

        /* retorna la respuesta en formato json */
        return response()->json($response, 200);
    }


    /**
     * Metodo para respuestas erroneas de las peticiones.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        /* Cuerpo de la respuesta */
    	  $response = [
            'success' => false,   // falso cuando falla
            'message' => $error,  // mensaje de error que retorna la peticion
        ];

        /* validacion diferente de vacio */
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        /* retorna la respuesta en formato json */
        return response()->json($response, $code);
    }
}
