<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Requests\Comments as CommentRequest;
use Illuminate\Http\Request;
use App\Models\Comments;

class CommentsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /* Se traen todos los comentarios */
      $comments = Comments::with('post','user')->paginate(10);
      /* Retorno los comentarios almacenados junto con el la estructura del sendResponse */
      return $this->sendResponse($comments->toArray(), 'Comentarios.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
      /* Se extrae la data del registro y se asigna a una nueva variable */
      $input    = $request->all();
      /* Se guarda el comentario ingresado */
      $comment = Comments::create($input);
      /* Retorno del registro junto con el la estructura del sendResponse */
      return $this->sendResponse($comment->toArray(), 'Comentario creado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* Busqueda del comentario especifico */
        $comment = Comments::find($id);
        /* Validacion si no encuentra el comentario */
        if (is_null($comment)) {
            return $this->sendError('No existe el comentario.');
        }
        /* Retorno del comentario especifico junto con el la estructura del sendResponse */
        return $this->sendResponse($comment->toArray(), 'Comentario retornado.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
      /* Busqueda del comentario especifico */
        $comment = Comments::find($id);
        /* Se extrae la data del comentario y se asigna a una nueva variable */
        $comment->update($request->all());
        /* Retorno del comentario especifico junto con el la estructura del sendResponse */
        return $this->sendResponse($comment->toArray(), 'Comentario Actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /* Busqueda del comentario especifico */
        $comment = Comments::find($request->id);
        /* Metodo para eliminar el comentario */
        $comment->delete();
        /* Retorno del comentario eliminado junto con el la estructura del sendResponse */
        return $this->sendResponse($comment->toArray(), 'Comentario Eliminado.');
    }
}
