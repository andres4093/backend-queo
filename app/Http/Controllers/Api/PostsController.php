<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Requests\Posts as PostsRequest;
use Illuminate\Http\Request;
use App\Models\Posts;

class PostsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /* Se traen todas las publicaciones */
      $posts = Posts::with('tag','user')->paginate(10);
      /* Retorna las publicaciones almacenadas junto con el la estructura del sendResponse */
      return $this->sendResponse($posts->toArray(), 'Publicaciones.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
      /* Se extrae la data del registro y se asigna a una nueva variable */
      $input    = $request->all();
      /* Se guarda la publicacion ingresada */
      $post      = Posts::create($input);
      /* Retorno de la publicacion junto con el la estructura del sendResponse */
      return $this->sendResponse($post->toArray(), 'Publicacion creada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* Busqueda de la publicacion especifica */
        $post = Posts::find($id);
        /* Validacion si no encuentra la publicacion */
        if (is_null($post)) {
            return $this->sendError('No existe la publicacion.');
        }
        /* Retorno de la publicacion especifica junto con el la estructura del sendResponse */
        return $this->sendResponse($post->toArray(), 'Publicacion retornada.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, $id)
    {
        /* Busqueda de la publicacion especifica */
        $post = Posts::find($id);
        /* Se extrae la data de la publicacion y se asigna a una nueva variable */
        $post->update($request->all());
        /* Retorno de la publicacion especifica junto con el la estructura del sendResponse */
        return $this->sendResponse($post->toArray(), 'Publicacion Actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /* Busqueda de la publicacion especifica */
        $post = Posts::find($request->id);
        /* Metodo para eliminar la publicacion */
        $post->delete();
        /* Retorno de la publicacion eliminada junto con el la estructura del sendResponse */
        return $this->sendResponse($post->toArray(), 'Publicacion Eliminada.');
    }
}
