<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Posts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validador de datos requeridos y tipo de datos permitidos
     *
     * @return array
     */
    public function rules()
    {
        return [
          'userId' => 'required',
          'tagId'  => 'required',
          'name'   => 'required|string',
          'email'  => 'required|email',
        ];
    }
}
