<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
  protected $table          = 'comments';   // Nombre de la tabla
  protected $primaryKey     = 'id';         // Llave primaria
  public    $timestamps     = true;        // Si tiene o no created_at y updated_at

  /* Campos de la tabla comments */
  protected $fillable = [
      'id',
      'postId',
      'userId',
      'name',
      'email',
      'body',
  ];

  /* Campos de fecha */
  protected $dates = [
      'created_at',
      'updated_at',
  ];

  /* Obtener el Id de la tabla */
  public function getIdAttribute() {
      return $this->attributes['id'];
  }

  /* Relacion con post */
  public function post() {
      return $this->belongsTo('App\Models\Posts', 'postId', 'id');
  }

  /* Relacion con usuario */
  public function user() {
      return $this->belongsTo('App\User', 'userId', 'id');
  }

}
