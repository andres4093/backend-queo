<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
  protected $table          = 'tags';   // Nombre de la tabla
  protected $primaryKey     = 'id';     // Llave primaria
  public    $timestamps     = true;    // Si tiene o no created_at y updated_at

  /* Campos de la tabla comments */
  protected $fillable = [
      'id',
      'name',
  ];

  /* Campos de fecha */
  protected $dates = [
      'created_at',
      'updated_at',
  ];

  /* Obtener el Id de la tabla */
  public function getIdAttribute() {
      return $this->attributes['id'];
  }

}
