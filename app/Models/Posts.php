<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $table          = 'posts';   // Nombre de la tabla
  protected $primaryKey     = 'id';      // Llave primaria
  public    $timestamps     = true;     // Si tiene o no created_at y updated_at

  /* Campos de la tabla posts */
  protected $fillable = [
      'id',
      'tagId',
      'userId',
      'name',
      'email',
  ];

  /* Campos de fecha */
  protected $dates = [
      'created_at',
      'updated_at',
  ];

  /* Obtener el Id de la tabla */
  public function getIdAttribute() {
      return $this->attributes['id'];
  }

  /* Relacion con post */
  public function tag() {
      return $this->hasMany('App\Models\Tags', 'id', 'tagId');
  }

  /* Relacion con usuario */
  public function user() {
      return $this->belongsTo('App\User', 'userId', 'id');
  }
}
