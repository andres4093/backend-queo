<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\RegisterController@register');

Route::middleware('auth:api')->group( function () {

    /* Comments */
    Route::prefix('comments')->name('comments')->group(function () {
      Route::get('/', 'CommentsController@index')->name('index');
      Route::post('/store', 'CommentsController@store')->name('store');
      Route::get('/show/{id}', 'CommentsController@show')->name('show');
      Route::put('/update/{id}', 'CommentsController@update')->name('update');
      Route::delete('/destroy/{id}', 'CommentsController@destroy')->name('destroy');
    });

    /* Posts */
    Route::prefix('posts')->name('posts')->group(function () {
      Route::get('/', 'PostsController@index')->name('index');
      Route::post('/store', 'PostsController@store')->name('store');
      Route::get('/show/{id}', 'PostsController@show')->name('show');
      Route::put('/update/{id}', 'PostsController@update')->name('update');
      Route::delete('/destroy/{id}', 'PostsController@destroy')->name('destroy');
    });

    /* Tags */
    Route::prefix('tags')->name('tags')->group(function () {
      Route::get('/', 'TagsController@index')->name('index');
      Route::post('/store', 'TagsController@store')->name('store');
      Route::get('/show/{id}', 'TagsController@show')->name('show');
      Route::put('/update/{id}', 'TagsController@update')->name('update');
      Route::delete('/destroy/{id}', 'TagsController@destroy')->name('destroy');
    });

});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
